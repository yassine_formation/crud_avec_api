let selectedRow = null;

function onFormSubmit(e) {
    event.preventDefault();
    const student = readData();
    if (selectedRow == null) {
        insertAndRefreshList(student);
    }
    else {
        updateAndRefreshList(student);
    }
    resetForm();
}

// lire des données 
function readData() { 
    const id_student = document.getElementById('id_student').value;
    const lastname = document.getElementById('lastname').value;
    const firstname = document.getElementById('firstname').value;
    const numberId = document.getElementById('numberId').value;
    const birthdate = document.getElementById('birthdate').value;
    const newStudent = new Student(id_student, lastname, firstname, numberId, birthdate);
    console.log(newStudent);
    return newStudent;
}


// fonction pour insérer les données
async function insertRecord(student) {
    await createStudent(student)
}
async function insertAndRefreshList(student) {
    await insertRecord(student);

    let tableBody = document.querySelector('#data-table');
    while(tableBody.firstChild) {
        tableBody.removeChild(tableBody.firstChild)
    }
    await fetchShowStudent();
}
// Editer les données dans le formulaire
function onEdit(td) {
    selectedRow = td.parentElement.parentElement;
    document.getElementById('id_student').value = selectedRow.cells[0].innerHTML;
    document.getElementById('lastname').value = selectedRow.cells[1].innerHTML;
    document.getElementById('firstname').value = selectedRow.cells[2].innerHTML;
    document.getElementById('numberId').value = selectedRow.cells[3].innerHTML;
    document.getElementById('birthdate').value = selectedRow.cells[4].innerHTML;
    console.log('fait ! ')
}




// fonction pour mettre a jour
async function updateRecord(student) {
    await updateStudent(student);
}
async function updateAndRefreshList(student) {
    // Fonction qui met à jour la base de données (ajout, modification ou suppression)
    await updateRecord(student);
    
    // Supprime tous les éléments de la table
    let tableBody = document.querySelector("#data-table");
    while (tableBody.firstChild) {
        tableBody.removeChild(tableBody.firstChild);
    }
    
    // Appelle la fonction fetchShowStudent pour afficher la nouvelle liste
    await fetchShowStudent();
}





// fonction pour supprimer les données 


async function onDelete(id_student, td) {
    if (confirm('Voulez-vous vraiment supprimer cet étudiant ?')) {
        let row = td.parentElement.parentElement;
        document.getElementById('myTable').deleteRow(row.rowIndex);      
        await deleteStudent(id_student);       
        resetForm();
    }
}

  

// fonction pour réinitialiser les données
function resetForm() {
    let inputFields = document.querySelectorAll('#formId input');
    for (let i = 0; i < inputFields.length; i++) {
        inputFields[i].value = '';
    }
    if (selectedRow) {
        selectedRow.classList.remove('selected');
        selectedRow = null;
    }
}

// class Student
class Student {
    constructor(id_student, lastname, firstname, numberId, birthdate, age) {
        this.id_student = id_student;
        this.lastname = lastname;
        this.firstname = firstname;
        this.numberId = numberId;
        this.birthdate = birthdate;
        this.age = age;
    }
}
const url_API = 'http://localhost:8080'

async function fetchShowStudent(student){
    fetch(url_API + '/student')
        .then(response => response.json())
        .then(data => {
            data.data.forEach(data => {

                let body = document.querySelector("#data-table");
                let row = document.createElement("tr");

                let id_student = document.createElement('td');
                id_student.innerHTML = data.id_student;

                let lastname = document.createElement("td");
                lastname.innerHTML = data.lastname;

                let firstname = document.createElement("td");
                firstname.innerHTML = data.firstname;

                let numberId = document.createElement('td');
                numberId.innerHTML = data.numberId;

                let birthdate = document.createElement('td');
                birthdate.innerHTML = data.birthdate;

                let age = document.createElement('td');
                age.innerHTML = data.age;

                let button = document.createElement('td');
                button.innerHTML = `<button class='edit' id = 'edit' onclick='onEdit(this)'>Edit</button> <button id = '${data.id_student}' class='delete' onclick='onDelete(${data.id_student},this)'>Delete</button> `;

                row.appendChild(id_student);
                row.appendChild(lastname);
                row.appendChild(firstname);
                row.appendChild(numberId);
                row.appendChild(birthdate);
                row.appendChild(age);
                row.appendChild(button);

                body.appendChild(row);
                
            });
        })
        .catch(error => console.log(error))
}

document.addEventListener('DOMContentLoaded', function() {
    fetchShowStudent();
})



async function createStudent(student) {
    try {
      const response = await fetch('http://localhost:8080/create', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(student)
      });
      
      if (!response.ok) {
        throw new Error('Network response was not ok');
      }
  
      const createdStudent = await response.json();
      return createdStudent.data;
    } catch (error) {
      console.error(error);
    }
  }
  
  
  async function updateStudent(student) {
    return fetch(url_API + '/edit/' + student.id_student, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(student)
    })
    .then(response => response.json())
}

async function deleteStudent(id_student) {
    // if (!student) return;
    const response = await fetch(`${url_API}/delete/${id_student}`, {
        method: 'DELETE',
    })
    .then(response => response.json());
}

